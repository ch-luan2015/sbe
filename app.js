const express = require('express');
const app = express();
const cors = require('cors');
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')

const errorMiddleware = require('./middlewares/errors')

// Setting up config file
if (process.env.NODE_ENV !== "PRODUCTION") require("dotenv").config({ path: "config/config.env" });

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser())
app.use(fileUpload());

app.use(cors({
  origin: true,
  methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PUT', 'PATCH'],
  credentials: true,

}));
// Import all routes
const products = require("./routes/product");
const auth = require("./routes/auth");
const order = require("./routes/order")
const payment = require("./routes/payment");


app.use("/api", products);
app.use("/api", auth);
app.use("/api", order);
app.use("/api", payment);


app.get("/", (req, res, next) => {
  res.status(200).json({
    success: true,
    messege: "Hello"
  })
})




//Middle to handler error
app.use(errorMiddleware);

module.exports = app;
