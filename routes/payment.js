const express = require('express')
const router = express.Router();


const {
  processPayment,
  sendStripApi,
  paymentMoMo,
  notifyMomo,
  confirmMomo
} = require('../controllers/paymentController')

const { isAuthenticatedUser } = require('../middlewares/auth')

router.route('/payment/process').post(isAuthenticatedUser, processPayment);
router.route('/stripeapi').get(isAuthenticatedUser, sendStripApi);


router.route('/payment/momo/pay').post(isAuthenticatedUser, paymentMoMo);
router.route('/payment/momo/notify').get(isAuthenticatedUser, notifyMomo);

router.route('/payment/momo/confirm').get(isAuthenticatedUser, confirmMomo);

module.exports = router;
