const Product = require("../models/product");
const dotenv = require("dotenv");

const productsSeed = require("../data/products.json");
const connectDatabase = require("../config/database");

//Settings dotenv file
dotenv.config({ path: "config/config.env" });
connectDatabase();

const seedProducts = async () => {

  try {
    await Product.countDocuments(async (err,count)=>{
      if(!err && count === 0){
          await Product.insertMany(productsSeed);
          console.log("No Collections ,Products are Added 2")
      } return
    })
    // await Product.deleteMany();
    console.log("Has collections, no need seed");
    
  } catch (error) {
    console.log(error.message);
  }
};


module.exports = seedProducts;
