const catchAsyncErrors = require('../middlewares/catchAsyncErrors')
const crypto = require('crypto');
const https = require("https");
const { v1: uuidv1 } = require('uuid');


const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

// Process stripe payments   =>   /api/v1/payment/process

exports.processPayment = catchAsyncErrors(async (req, res, next) => {

  const paymentIntent = await stripe.paymentIntents.create({
    amount: req.body.amount,
    currency: "usd",
    metadata: { integration_check: "accept_a_payment" }
  });

  res.status(200).json({
    success: true,
    client_secret: paymentIntent.client_secret
  })


});

// Send stripe API Key   =>   /api/v1/stripeapi
exports.sendStripApi = catchAsyncErrors(async (req, res, next) => {

  res.status(200).json({
    stripeApiKey: process.env.STRIPE_API_KEY
  })

})



// Process Momo payments   =>   /api/v1/payment/momo
exports.paymentMoMo = catchAsyncErrors(async (req, res, next) => {


  //1
  //parameters send to MoMo get get payUrl
  var endpoint = "https://test-payment.momo.vn/gw_payment/transactionProcessor"
  var hostname = "https://test-payment.momo.vn"
  var path = "/gw_payment/transactionProcessor"
  var partnerCode = "MOMOKDJ520210810"
  var accessKey = "dfbCh9Bpbg3FKceA"
  var serectkey = "SZBS3DttjMZdrOFbkPxQZW5BDTmOquqc"
  var orderInfo = "pay with MoMo"
  var returnUrl = "http://localhost:3000/success"
  var notifyurl = "http://localhost:6000/api/v1/payment/momo/notify"
  var amount = req.body.amount.toString();
  var orderId = req.body.orderId.toString();
  var requestId = uuidv1()
  var requestType = "captureMoMoWallet"
  var extraData = "key=test1"

  //2
  var rawSignature = "partnerCode=" + partnerCode + "&accessKey=" + accessKey + "&requestId=" + requestId + "&amount=" + amount + "&orderId=" + orderId + "&orderInfo=" + orderInfo + "&returnUrl=" + returnUrl + "&notifyUrl=" + notifyurl + "&extraData=" + extraData
  //signature
  var signature = crypto.createHmac('sha256', serectkey)
    .update(rawSignature)
    .digest('hex');

  //3 Body send
  // json object send to MoMo endpoint
  var body = JSON.stringify({
    partnerCode: partnerCode,
    accessKey: accessKey,
    requestId: requestId,
    amount: amount,
    orderId: orderId,
    orderInfo: orderInfo,
    returnUrl: returnUrl,
    notifyUrl: notifyurl,
    extraData: extraData,
    requestType: requestType,
    signature: signature,
  })

  // Create the HTTPS objects
  var options = {
    hostname: 'test-payment.momo.vn',
    port: 443,
    path: '/gw_payment/transactionProcessor',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(body)
    }
  };


  //Send the request and get the response
  var reqMomo = await https.request(options, (resMomo) => {
    resMomo.setEncoding('utf8');

    resMomo.on('data', (body) => {
      // console.log('Body');
      // console.log(body);
      // console.log('payURL');
      // console.log(JSON.parse(body).payUrl);
      var urlMomo = JSON.parse(body).payUrl;
      res.setHeader('Content-Type', 'text/plain');
      res.status(200).json({
        urlMomo,
      })

    })


    res.on('end', () => {
      console.log('Body: ', JSON.parse(body));
    });

  }).on("error", (err) => {
    console.log("Error: ", err.message);
  });


  // write data to request body
  reqMomo.write(body);
  reqMomo.end();

})

exports.notifyMomo = catchAsyncErrors(async (req, res, next) => {
  var data = Object.assign([], req.query);

  console.log("req", req.query)
  data.isSuccess = false;
  if (req.query.errorCode == '0') {
    data.isSuccess = true;
  }

  res.status(200).json({
    data: data,

  })
})


exports.confirmMomo = catchAsyncErrors(async (req, res, next) => {

  var data = Object.assign([], req.query);
  data.isSuccess = false;
  if (req.query.errorCode == '0') {
    data.isSuccess = true;
  }

  res.status(200).json({
    payload: req.payload,
    body: req.body,
    query: req.query,
    data: data,
  })
})

