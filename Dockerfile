FROM node:14-slim

WORKDIR /app

COPY . .

RUN npm install

# Development
CMD ["npm", "run", "start", "--env"]

# Production
# RUN npm install global pm2
# CMD ["pm2-runtime", "ecosystem.config.js", "--env", "production"]

EXPOSE 5005
EXPOSE 27017
